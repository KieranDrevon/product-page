@extends('layouts.app')

@section('content')
    <div class="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
        @foreach ($products as $product)
            <div class="bg-white p-4 shadow-md rounded-md">
                <img class="w-32 h-32 mx-auto mb-4 object-contain" src="{{ $product->image }}" alt="{{ $product->title }}">
                <h2 class="text-lg font-semibold">
                    <a href="/product/{{ $product->id }}" class="text-blue-500 hover:underline">
                        {{ $product->title }}
                    </a>
                </h2>
                <p class="text-gray-600">{{ ucwords($product->category) }}</p>
                <p class="text-blue-500 font-bold">${{ $product->price }}</p>
            </div>
        @endforeach
    </div>
@endsection
