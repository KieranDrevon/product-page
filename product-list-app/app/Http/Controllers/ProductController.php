<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    // List all products
    public function getAllProducts() {
        $products = Product::all();
        return view('products', ['products' => $products]);
    }

    public function getSingleProduct($id) {
        $product = Product::findOrFail($id);
        return view("product", ['product' =>  $product]);
    }

    public function createProduct(Request $request) {
        $data = $request->validate([
            'title' => 'required',
            'price' => 'required|numeric',
            'category' => 'required',
            'description' => 'required',
            'image' => 'required|url',
        ]);

        Product::create($data);

        return redirect('products');
    }
}
