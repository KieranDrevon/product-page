<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::redirect('/', '/products');


Route::get('/products', [ProductController::class, 'getAllProducts']);

Route::post('/products/create', [ProductController::class, 'createProduct'])->name('product.create');

Route::get('/products/create', function () {
    return view('create');
});

Route::get("/product/{id}", [ProductController::class, 'getSingleProduct']);

