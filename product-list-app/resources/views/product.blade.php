@extends('layouts.app')

@section('content')

    <div class="bg-white rounded-lg shadow-lg p-6">
        <div class="flex items-center justify-center">
        <img class="w-64 h-64 object-contain" src="{{ $product->image }}" alt="{{ $product->title }}">
        </div>
        <div class="mt-4">
            <h1 class="text-2xl font-semibold">{{ $product->title }}</h1>
            <p class="text-gray-600">{{ ucwords($product->category) }}</p>
            <p class="text-lg font-bold text-blue-500">${{ $product->price }}</p>
            <p class="mt-4">{{ $product->description }}</p>
        </div>
    </div

@endsection
