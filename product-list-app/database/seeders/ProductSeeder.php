<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Hit the FakeStoreAPI endpoint to seed our DB with products

        $response = Http::get('https://fakestoreapi.com/products');

        $productsData = $response->json();

        foreach ($productsData as $productData ) {
            $product = new Product([
                'title' => $productData['title'],
                'price' => $productData['price'],
                'category' => $productData['category'],
                'description' => $productData['description'],
                'image' => $productData['image'],
            ]);

            $product->save();
        }
    }
}
