<!DOCTYPE html>
<html>
<head>
    <title>Practice Project</title>
    @vite('resources/css/app.css')
</head>
<body class="bg-grey-100">

<nav class="bg-gradient-to-r from-blue-500 to-purple-500 p-4">
    <div class="container mx-auto flex justify-between items-center">
        <a class="text-black text-2xl font-bold" href="/">Laravel Practice Project</a>
        <ul class="flex space-x-6">
            <li><a class="text-black hover:text-white mr-4" href="/products">All Products</a></li>
            <li><a class="text-black hover:text-white mr-4" href="/products/create">Create Product</a></li>

        </ul>
    </div>
</nav>

<div class="container mx-auto p-8">
    @yield('content')
</div>

</body>
</html>



