@extends('layouts.app')

@section('content')
    <div class="container mx-auto p-8">
        <h1 class="text-2xl font-semibold mb-4">Create New Product</h1>
        <form action="{{ route('product.create') }}" method="post" class="max-w-lg mx-auto">
            @csrf
            <div class="mb-4">
                <label for="title" class="block text-gray-700 font-semibold mb-2">Title</label>
                <input type="text" id="title" name="title" class="w-full border rounded p-2">
            </div>
            <div class="mb-4">
                <label for="price" class="block text-gray-700 font-semibold mb-2">Price</label>
                <input type="number" id="price" name="price" class="w-full border rounded p-2">
            </div>
            <div class="mb-4">
                <label for="category" class="block text-gray-700 font-semibold mb-2">Category</label>
                <input type="text" id="category" name="category" class="w-full border rounded p-2">
            </div>
            <div class="mb-4">
                <label for="description" class="block text-gray-700 font-semibold mb-2">Description</label>
                <textarea id="description" name="description" rows="4" class="w-full border rounded p-2"></textarea>
            </div>
            <div class="mb-4">
                <label for="image" class="block text-gray-700 font-semibold mb-2">Image URL</label>
                <input type="text" id="image" name="image" class="w-full border rounded p-2">
            </div>
            <button type="submit" class="text-black font-bold px-4 py-2  hover:bg-sky-500 border">
                Create Product
            </button>
        </form>
    </div>
@endsection
